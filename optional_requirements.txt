## To install these requirements, run
## pip install -U -r optional_requirements.txt
## (the -U option also upgrades packages; from the second time on,
## just run
## pip install -r optional_requirements.txt
##
## NOTE: before running the command above, you need to install a recent version
## of pip from the website, and then possibly install/upgrade setuptools using
## sudo pip install --upgrade setuptools

## For postgreSQL
psycopg2==2.6

## For MySQL
MySQL-python==1.2.5

## To have a decent recent version of sqlite
## Note that django uses pysqlite instead of the system-provided
## sqlite3, if pysqlite is available: django/db/backends/sqlite3/base.py
## This is because we need recursive triggers in sqlite, present
## only after sqlite 3.6.18
pysqlite==2.6.3

# Support for NWChem I/O
pymatgen

## pyspglib for symmetry detection in aiida.orm.data.structure
pyspglib

## Support for the AiiDA CifData class
PyCifRW==3.6.2.1
