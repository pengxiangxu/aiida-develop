from aiida.orm.workflow import Workflow
from aiida.orm.data.structure import StructureData
from aiida.orm.calculation.inline import make_inline
from aiida.common.exceptions import *
from aiida.orm.calculation.job.quantumespresso.pw import PwCalculation 
from aiida.orm.calculation.job.exciting.exciting import ExcitingCalculation
from aiida.orm.group import Group

# tested to run with QE and Exciting plugins

class Workflow_EOS(Workflow):

    def __init__(self, **kwargs):

        super(Workflow_EOS, self).__init__(**kwargs)

    def scaled_structure(self, structure, scale):

        import numpy as np

        new_structure = StructureData(cell=np.array(structure.cell)*scale)

        for site in structure.sites:
            new_structure.append_atom(position=np.array(site.position)*scale, \
                                      symbols=structure.get_kind(site.kind_name).symbol,\
                                      name=site.kind_name)
        new_structure.label = ''
        new_structure.description = "auxiliary structure for EOS "\
                                    "created from the original structure with PK=%i, "\
                                    "lattice constant scaling: %f"%(structure.pk, scale)

        return new_structure

    #@make_inline
    ##def rescale_inline(self, structure, scale):
    #def rescale_inline(**kwargs):
    #    """
    #    Inline calculation to rescale a structure
    #    :param parameters: a ParameterData object with a dictionary of the form
    #        {'scaling_ratio': 1.02} (here for a 2% expansion)
    #    :param structure: an AiiDA structure to rescale
    #    :return: a dictionary of the form
    #        {'rescaled_structure': new_rescaled_structure}
    #    """
    #    import numpy as np
    #
    #    new_structure = StructureData(cell=np.array(structure.cell)*scale)

    #    for site in structure.sites:
    #        new_structure.append_atom(position=np.array(site.position)*scale, \
    #                                  symbols=structure.get_kind(site.kind_name).symbol,\
    #                                  name=site.kind_name)
    #
    #    return {'rescaled_structure': new_structure}

    def create_calculation(self, structure_new):

        # get parameters of the workflow
        params = self.get_parameters()

        code = params['code']

        # create new calculation
        calc = code.new_calc()

        # set label and description
        calc.label = 'called from EOS plugin'
        calc.description = calc.label

        calc.use_structure(structure_new)

        if isinstance(calc, PwCalculation):
            calc.use_pseudos_from_family(params['pseudo_family'])
            calc.use_settings(params['calculation_settings'])
        elif isinstance(calc, ExcitingCalculation):
            calc.use_lapwbasis_from_family(params['lapw_family'])
        else:
            raise AiidaException("Wrong calculation")

        calc.use_parameters(params['calculation_parameters'])
        calc.use_kpoints(params['kpoints'])
            
        calc.set_max_wallclock_seconds(params['calculation_wallclock_seconds'])
        calc.set_environment_variables(params['environment_variables'])
        calc.set_resources(params['calculation_resources'])
        calc.set_mpirun_extra_params(params['mpirun_extra_params'])
        calc.store()
        
        return calc
        
    @Workflow.step
    def start(self):

        # get a dictionary with workflow parameters
        params = self.get_parameters()

        if not self.label:
            raise NotExistent("Workflow label is not provided")

        if not self.description:
            raise NotExistent("Workflow description is not provided")
            
        struct = params['structure']
        code = params['code']

        self.append_to_report("structure PK: {}".format(struct.pk))
        self.append_to_report("code label: {}".format(code.label))

        self.next(self.eos)

    @Workflow.step
    def eos(self):

        import numpy as np
        
        # get parameters of the workflow
        params = self.get_parameters()

        # get original structure
        structure = params['structure']

        # volume scales from 0.94 to 1.06, alat scales as pow(1/3)
        scales = np.linspace(0.979586108715562, 1.019612822422216, num=params['num_points']).tolist()

        for scale in scales:

            structure_new = self.scaled_structure(structure, scale)
            structure_new.store()

            calc = self.create_calculation(structure_new)

            self.attach_calculation(calc)

        self.next(self.post_process)

    @Workflow.step
    def post_process(self):

        # get calculations
        eos_calcs = self.get_step_calculations(self.eos)

        e_calcs = [c.res.energy for c in eos_calcs]
        v_calcs = [c.res.volume for c in eos_calcs]
        
        self.add_result('eos', sorted(zip(v_calcs, e_calcs)))

        params = self.get_parameters()
        if params['group']:
            from aiida.orm.data.parameter import ParameterData
            grp, created = Group.get_or_create(name=params['group'])
            result_node = ParameterData(dict={'eos'          : sorted(zip(v_calcs, e_calcs)),
                                              'structure_PK' : params['structure'].pk,
                                              'code_PK'      : params['code'].pk,
                                              'workflow_PK'  : self.pk})
            result_node.store()
            grp.add_nodes([result_node])

        self.next(self.exit)


