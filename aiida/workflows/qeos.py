#import aiida.common
#from aiida.common import aiidalogger
from aiida.orm.workflow import Workflow
from aiida.orm.data.structure import StructureData
from aiida.orm.calculation.inline import make_inline
#from aiida.orm import Code, Computer
#from aiida.orm import CalculationFactory, DataFactory

class QeosWorkflow(Workflow):

    def __init__(self, **kwargs):

        super(QeosWorkflow, self).__init__(**kwargs)

    def scaled_structure(self, struct, scale):

        cell = struct.cell

        for i in range(3):
            for j in range(3):
                cell[i][j] *= scale

        struct_new = StructureData(cell=cell)

        for site in struct.sites:
            pos = [scale * x for x in site.position]
            struct_new.append_atom(position=pos, symbols=site.kind_name)

        return struct_new

    #@make_inline
    #def rescale_inline(self, structure, scale):
    #    """
    #    Inline calculation to rescale a structure
    #    :param parameters: a ParameterData object with a dictionary of the form
    #        {'scaling_ratio': 1.02} (here for a 2% expansion)
    #    :param structure: an AiiDA structure to rescale
    #    :return: a dictionary of the form
    #        {'rescaled_structure': new_rescaled_structure}
    #    """
    #    import numpy as np
    #
    #    new_structure = StructureData(cell=np.array(structure.cell)*scale)

    #    for site in structure.sites:
    #        new_structure.append_atom(position=np.array(site.position)*scale, \
    #                                  symbols=structure.get_kind(site.kind_name).symbol,\
    #                                  name=site.kind_name)
    #
    #    return {'rescaled_structure': new_structure}

    def create_calculation(self, params, struct):

        code = params['code']
        
        calc = code.new_calc()

        calc.set_max_wallclock_seconds(60*60) # in seconds
        calc.use_structure(struct)
        
        if 'lapw_family' in params:
            calc.use_lapwbasis_from_family(params['lapw_family'])

        if 'pseudo_family' in params:
            calc.use_pseudos_from_family(params['pseudo_family'])
        
        if 'code_parameters' in params:
            calc.use_parameters(params['code_parameters'])

        calc.use_kpoints(params['kpoints'])

        if 'code_settings' in params:
            calc.use_settings(params['code_settings'])

        calc.label = 'called from EOS plugin'
        calc.description = calc.label
        calc.set_environment_variables(params['environment_variables'])
        calc.set_resources(params['resources'])
        calc.set_mpirun_extra_params(params['mpirun_extra_params'])
        calc.store()
        return calc
        
    @Workflow.step
    def start(self):

        params = self.get_parameters()
        struct = params['structure']
        code = params['code']

        self.append_to_report("structure PK: {}".format(struct.pk))
        self.append_to_report("code label: {}".format(code.label))

        self.next(self.eos)

    @Workflow.step
    def eos(self):

        import numpy as np

        params = self.get_parameters()
        s = params['structure']

        # volume scales from 0.94 to 1.06, alat scales as pow(1/3)
        scales = np.linspace(0.979586108715562, 1.019612822422216, num=params['num_points']).tolist()
        
        #exclusively for Cu case study
        #scales = np.linspace(0.979586108715562 / 1.03232756173716, 1.019612822422216 / 1.03232756173716, num=params['num_points']).tolist()

        self.append_to_report("scales: {}".format(scales))

        for scale in scales:
            #out = self.rescale_inline(self, s, scale)
            #snew = out['rescaled_structure']

            snew = self.scaled_structure(s, scale)
            snew.store()

            calc = self.create_calculation(params, snew)

            self.attach_calculation(calc)

        self.next(self.post_process)

    @Workflow.step
    def post_process(self):

        # Get calculations
        start_calcs = self.get_step_calculations(self.eos)

        e_calcs = [c.res.energy for c in start_calcs]
        v_calcs = [c.res.volume for c in start_calcs]
        
        self.add_result("eos", sorted(zip(v_calcs, e_calcs)))

        self.next(self.exit)


